.venv/bin/activate: requirements.txt
	python3 -m venv .venv
	./.venv/bin/pip install -r requirements.txt

run: .venv/bin/activate
	./.venv/bin/python3 -m sync_server.run

test: .venv/bin/activate
	./.venv/bin/python3 -m unittest discover -p "*_test.py" -v

clean:
	rm -r .venv