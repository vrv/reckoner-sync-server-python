import unittest
from sqlalchemy.ext.asyncio import AsyncSession
from ...database import crud
from ...database.connection import Reset, engine
from ...database.schema import UserNew
from ...database.model import User as UserDB
from ...password import verify

class TestUserCrud(unittest.IsolatedAsyncioTestCase):
	session: AsyncSession
	
	async def asyncSetUp(self):
		await Reset()
		self.session = AsyncSession(engine)

	async def asyncTearDown(self):
		await self.session.close()
	
	async def test_create_success(self):
		user = UserNew(
			name = "test",
			email = "noreply@noname.com",
			password = "password",
			is_admin = True,
			totp_secret = "shhh",
			public_key = "lock",
			private_key = "key",
		)

		user_expected = UserDB(**user.dict())
		
		user_result = await crud.User.create(self.session, user)
		self.assertIsInstance(user_result, UserDB)
		self.assertGreaterEqual(user_result.id, 1)
		user_expected.id = user_result.id
		self.assertTrue(await verify("password", user_result.password))
		user_result.password = user_expected.password
		self.assertEqual(user_expected, user_result)
		

	async def test_read_success(self):
		user = UserNew(
			name = "read",
			email = "read@noname.com",
			password = "password"
		)
		user_expected = await crud.User.create(self.session, user)
		
		# Read ID
		user_result = await crud.User.readID(self.session, user_expected.id)
		self.assertEqual(user_expected, user_result)

		#Read name
		user_result = await crud.User.readName(self.session, user.name)
		self.assertEqual(user_expected, user_result)

		#Read email
		user_result = await crud.User.readEmail(self.session, user.email)
		self.assertEqual(user_expected, user_result)

	async def test_totp(self):
		user = UserNew(
			name = "totp",
			email = "totp@noname.com",
			password = "password"
		)
		user_expected = await crud.User.create(self.session, user)
		ID = user_expected.id
		totp_secret: str = await crud.User.updateTotp(self.session, ID)
		self.assertTrue(len(totp_secret) > 0)
		user_result = await crud.User.readID(self.session, ID)
		self.assertEqual(user_expected, user_result)
		self.assertTrue(await crud.User.deleteTotp(self.session, ID))
		user_result = await crud.User.readID(self.session, ID)
		self.assertIsNone(user_result.totp_secret)

	async def test_update_success(self):
		user_in = UserNew(
			name = "update",
			email	= "update@noname.com",
			password = "password"
		)
		user_expected = await crud.User.create(self.session, user_in)
		ID = user_expected.id

		user = UserNew(
			name = "update_2",
			email = "update_2@noname.com",
			password = "1337"
		)
		user_expected.name = user.name
		user_expected.email = user.email
		user_expected.password = user.password

		success = await crud.User.update(self.session, ID, user)
		self.assertTrue(success)
		user_result = await crud.User.readID(self.session, user_expected.id)
		self.assertEqual(user_expected, user_result)

	async def test_delete_success(self):
		user = UserNew(
			name = "delete",
			email = "delete@noname.com",
			password = "password"
		)
		user_db = await crud.User.create(self.session, user)
		ID: int = user_db.id
		await crud.Item.create(self.session, ID, [{"test": "test"}, {"blah": "blah"}])
		success = await crud.User.delete(self.session, ID)
		self.assertTrue(success)
		await self.session.flush()
		user_db = await crud.User.readID(self.session, ID)
		self.assertIsNone(user_db)
		item_db_list = await crud.Item.read(self.session, ID)
		self.assertEqual(len(item_db_list), 0)


	async def test_create_fail(self):
		user = UserNew(
			name = "create_fail",
			email = "create_fail@noname.com",
			password = "password"
		)
		user_db = await crud.User.create(self.session, user)
		self.assertIsNotNone(user_db)
		user_db = await crud.User.create(self.session, user)
		self.assertIsNone(user_db)

	async def test_read_fail(self):
		user_db = await crud.User.readID(self.session, 1000)
		self.assertIsNone(user_db)
		user_db = await crud.User.readName(self.session, "blah")
		self.assertIsNone(user_db)
		user_db = await crud.User.readID(self.session, "blah@blah.blah")
		self.assertIsNone(user_db)
	
	async def test_update_fail(self):
		user = UserNew(
			name = "update",
			email	= "update@noname.com",
			password = "password"
		)
		success = await crud.User.update(self.session, 1000, user)
		self.assertFalse(success)

	async def test_delete_fail(self):
		success = await crud.User.delete(self.session, 1000)
		self.assertFalse(success)

class TestItemCrud(unittest.IsolatedAsyncioTestCase):
	session: AsyncSession
	user_id: int
	
	async def asyncSetUp(self):
		await Reset()
		self.session = AsyncSession(engine)
		user_db = await crud.User.create(
			self.session,
			UserNew(
				name = "test",
				email = "noreply@noname.com",
				password = "password"
			)
		)
		self.user_id = user_db.id

	async def asyncTearDown(self):
		await self.session.close()

	async def test_create_success(self):
		item_list = [ {"test": "testTest"}, {"blah": "blahBlah"} ]
		item_db_list = await crud.Item.create(self.session, self.user_id, item_list)
		self.assertEqual(len(item_db_list), 2)
		result_list = []
		for index in range(2):
			item_db = item_db_list[index]
			self.assertIsNotNone(item_db.update_inst)
			self.assertGreaterEqual(item_db.id, 1)
			result_list.append(item_db.data)
		self.assertListEqual(item_list, result_list)

	async def test_read_success(self):
		await Reset()
		item_db_list_result = await crud.Item.create(self.session, self.user_id, [{"a": "a"}])
		item_db = item_db_list_result[0] 
		inst_a = item_db.update_inst
		self.assertEqual(item_db, await crud.Item.readID(self.session, self.user_id, item_db.id))
		item_db_list_result = await crud.Item.create(self.session, self.user_id, [{"b": "b"}, {"c": "c"}])
		inst_b = item_db_list_result[0].update_inst
		item_db_list_result = await crud.Item.read(self.session, self.user_id)
		self.assertEqual(len(item_db_list_result), 3)
		item_db_list_result = await crud.Item.read(self.session, -100)
		self.assertEqual(len(item_db_list_result), 0)
		item_db_list_result = await crud.Item.read(self.session, self.user_id, inst_a)
		self.assertEqual(len(item_db_list_result), 3)
		item_db_list_result = await crud.Item.read(self.session, self.user_id, inst_b)
		self.assertEqual(len(item_db_list_result), 2)

	async def test_update_success(self):
		item_db_list_result = await crud.Item.create(self.session, self.user_id, [{"test": "testTest"}])
		ID = item_db_list_result[0].id
		self.assertTrue(await crud.Item.update(self.session, self.user_id, {ID: {"blah": "blahBlah"}}))
		item_db = await crud.Item.readID(self.session, self.user_id, ID)
		self.assertEqual(item_db.data, {"blah": "blahBlah"})

	async def test_delete_success(self):
		item_db_list_result = await crud.Item.create(self.session, self.user_id, [{"test": "testDelete"}])
		ID = item_db_list_result[0].id
		success = await crud.Item.delete(self.session, self.user_id, ID)
		self.assertTrue(success)
		await self.session.flush()
		self.assertIsNone(await crud.Item.readID(self.session, self.user_id, ID))

	async def test_read_fail(self):
		item_list = await crud.Item.read(self.session, 1000)
		self.assertEqual(len(item_list), 0)
		self.assertIsNone(await crud.Item.readID(self.session, self.user_id, 1000))

	async def test_update_fail(self):
		self.assertFalse(await crud.Item.update(self.session, self.user_id, {1000: {"update": "fail"}}))
	
	async def test_delete_fail(self):
		self.assertFalse(await crud.Item.delete(self.session, self.user_id, 1000))

if __name__ == '__main__':
	unittest.main()
