import asyncio
from sqlalchemy.ext.asyncio import AsyncSession
from .config import Load as LoadConfig
from .database import crud
from .database.schema import UserNew
from .database.connection import GetEngine

LoadConfig()
session = AsyncSession(GetEngine())

async def _add_user(user: UserNew):
	await crud.User.create(session, user)
	await session.commit()
	
async def _delete_user(id: int):
	await crud.User.delete(session, id)
	await session.commit()

async def _get_user(id: int):
	return await crud.User.readID(session, id)

def AddUser():
	name = input("Enter an username: ")
	email = input("Enter an email: ")
	password = input("Enter a password: ")
	user = UserNew(name=name, email=email, password=password)
	if input("Make admin (y/n): ") == "y":
		user.is_admin = True
	asyncio.run(_add_user(user))	

def DeleteUser():
	id = int(input("User ID to delete: "))
	user_db = asyncio.run(_get_user(id))
	if not user_db:
		print("ID is not a user")
		return
	print(f"Name: {user_db.name}")
	print(f"Email: {user_db.email}")
	if input("Continue (y/n): ") == "y":
		asyncio.run(_delete_user(id))

if __name__ == '__main__':
	print("Choose action to take\n[A]dd\n[D]elete")
	choice = input("Choice: ")[0].lower()

	if choice == "a":
		AddUser()
	elif choice == "d":
		DeleteUser()
	asyncio.run(session.close())