from fastapi import APIRouter, HTTPException, Depends, status
from sqlalchemy.ext.asyncio import AsyncSession
from ..database.connection import GetSession
from ..database.schema import User, UserNew
from ..database import crud
from .security import getUid

router = APIRouter(prefix="/user")

@router.get("/", response_model=User)
async def get_current_user(
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	result = await crud.User.readID(session, uID)
	if (not result): HTTPException(400, "User not found")
	return result

@router.put("/", status_code=status.HTTP_204_NO_CONTENT)
async def update_current_user(
	user: UserNew,
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	result = await crud.User.update(session, uID, user)
	await session.commit()
	if (not result): HTTPException(400, "User does not exist")

@router.delete("/", status_code=status.HTTP_204_NO_CONTENT)
async def delete_current_user(
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	result = await crud.User.delete(session, uID) 
	await session.commit()
	if (not result): HTTPException(400, "User does not exist")

@router.get("/totp", response_model=str)
async def get_totp_secret(
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	totp_secret = await crud.User.updateTotp(session, uID)
	if (not totp_secret): HTTPException(400, "User does not exist")
	return totp_secret

@router.delete("/totp")
async def delete_totp_secret(
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	if not await crud.User.deleteTotp(session, uID):
		HTTPException(400, "User does not exist")