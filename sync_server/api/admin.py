from fastapi import APIRouter, HTTPException, Depends, status
from sqlalchemy.ext.asyncio import AsyncSession
from ..database.connection import GetSession
from ..database.schema import User, UserNew
from ..database import crud
from .security import getUid, isUserAdmin

router = APIRouter(prefix="/admin")

@router.post("/user/", response_model=User, status_code=status.HTTP_201_CREATED)
async def create_user(
	user: UserNew, 
	session: AsyncSession = Depends(GetSession), 
	uID: int = Depends(getUid)
):
	await isUserAdmin(session, uID)
	result = await crud.User.create(session, user)
	if (not result): HTTPException(400, "User already registered")
	await session.commit()
	return result

@router.get("/user/{ID}", response_model=User)
async def get_user_by_ID(
	ID: int, 
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	await isUserAdmin(session, uID)
	result = await crud.User.readID(session, ID)
	if (not result): HTTPException(400, "User not found")
	return result

@router.get("/user/name/{name}", response_model=User)
async def get_user_by_name(
	name: str,
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	await isUserAdmin(session, uID)
	result = await crud.User.readName(session, name)
	if (not result): HTTPException(400, "User not found")
	return result

@router.get("/user/email/{email}", response_model=User)
async def get_user_by_email(
	email: str,
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	await isUserAdmin(session, uID)
	result = await crud.User.readEmail(session, email)
	if (not result): HTTPException(400, "User not found")
	return result

@router.put("/user/{ID}", status_code=status.HTTP_204_NO_CONTENT)
async def update_user(
	ID: int,
	user: User,
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	await isUserAdmin(session, uID)
	result = await crud.User.update(session, ID, user)
	await session.commit()
	if (not result): HTTPException(400, "User does not exist")

@router.delete("/user/{ID}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(
	ID:int,
	session: AsyncSession = Depends(GetSession),
	uID: int = Depends(getUid)
):
	await isUserAdmin(session, uID)
	result = await crud.User.delete(session, ID) 
	await session.commit()
	if (not result): HTTPException(400, "User does not exist")
