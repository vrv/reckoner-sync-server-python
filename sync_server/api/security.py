from secrets import token_hex
from datetime import datetime, timedelta
from typing import Optional
from pyotp.totp import TOTP
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi import APIRouter, HTTPException, Depends, status, Form
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from pydantic import BaseModel
from ..database import crud
from ..database.connection import GetSession
from ..database.model import User
from ..password import verify

oauth2_scheme = OAuth2PasswordBearer("api/token")
SECRET_KEY = token_hex(32)
ALGORITHM = jwt.ALGORITHMS.HS256
EXPIRATION_DELTA_SHORT = timedelta(minutes=5)
EXPIRATION_DELTA_LONG = timedelta(days=30)
router = APIRouter()

class Token(BaseModel):
	access_token: str
	token_type: str
	expires_in: Optional[int]
	scope: Optional[str]

def _token(token: str, exp: int, scope: str = "all"):
	return {
		"access_token": token,
		"token_type": "bearer",
		"expires_in": exp,
		"scope": scope
	}

def _unauthorizedException(detail:str):
	return HTTPException(
		status_code=status.HTTP_401_UNAUTHORIZED,
		headers={"WWW-Authenticate": "Bearer"},
		detail=detail
	)

async def getUid(token: str = Depends(oauth2_scheme)) -> int:
	credentials_exception = _unauthorizedException("Could not validate credentials")
	uid: int = None
	try:
		payload = jwt.decode(token, SECRET_KEY, [ALGORITHM])
		uid = int(payload.get("sub"))
	except (JWTError, ValueError):
		raise credentials_exception
	return uid

async def _getUserName(token: str = Depends(oauth2_scheme)) -> str:
	credentials_exception = _unauthorizedException("User not logged in")
	username: str
	try:
		payload = jwt.decode(token, SECRET_KEY, [ALGORITHM])
		username = payload.get("sub")
	except (JWTError, ValueError):
		raise credentials_exception
	return username

async def isUserAdmin(session: AsyncSession, uid:int):
	user_db = await crud.User.readID(session, uid)
	if not user_db:
		raise _unauthorizedException("Could not find user")
	if not user_db.is_admin:
		raise _unauthorizedException("User is not an administrator")

def _createAccessToken(subject, expires_delta: timedelta) -> str:
	to_encode = {"sub": str(subject)}
	expire = datetime.utcnow() + expires_delta
	to_encode.update({"exp": expire})
	return jwt.encode(to_encode, SECRET_KEY, ALGORITHM)

@router.post("/token", response_model=Token)
async def login(
	form_data: OAuth2PasswordRequestForm = Depends(), 
	session: AsyncSession = Depends(GetSession)
):
	credentials_exception = _unauthorizedException("Incorrect username or password")
	user_db: User = await crud.User.readName(session, form_data.username)
	if not user_db:
		raise credentials_exception
	if not await verify(form_data.password, user_db.password):
		raise credentials_exception
	access_token: str
	expiration_seconds: int
	scope: str
	if user_db.totp_secret:
		access_token = _createAccessToken(user_db.name, EXPIRATION_DELTA_SHORT)
		expiration_seconds = EXPIRATION_DELTA_SHORT.seconds
		scope = "totp"
	else:
		access_token = _createAccessToken(user_db.id, EXPIRATION_DELTA_LONG)
		expiration_seconds = EXPIRATION_DELTA_LONG.seconds
		scope = "all"
	return _token(access_token, expiration_seconds, scope)

@router.post("/totp", response_model=Token)
async def get_totp(
	otp: str = Form(...),
	username: str = Depends(_getUserName),
	session: AsyncSession = Depends(GetSession)
):
	credentials_exception = _unauthorizedException("Missing validation information")
	user_db: User = await crud.User.readName(session, username)
	if not user_db:
		raise credentials_exception
	if not TOTP(user_db.totp_secret).verify(otp):
		raise credentials_exception
	access_token = _createAccessToken(user_db.id, EXPIRATION_DELTA_LONG)
	expiration_seconds = EXPIRATION_DELTA_LONG.seconds
	return _token(access_token, expiration_seconds)
