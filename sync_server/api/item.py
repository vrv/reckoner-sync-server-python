from typing import List, Dict, Optional
from datetime import datetime
from fastapi import APIRouter, HTTPException, Depends, Path, status
from sqlalchemy.ext.asyncio import AsyncSession
from ..database.connection import GetSession
from ..database.schema import Item
from ..database import crud
from .security import getUid

router = APIRouter(prefix="/item")

@router.post("/", response_model=List[Item], status_code=status.HTTP_201_CREATED)
async def create_items(
	item_list: List[Dict] = None,
	uID: int = Depends(getUid), 
	session: AsyncSession = Depends(GetSession)
):
	result = await crud.Item.create(session, uID, item_list)
	session.commit()
	return result

@router.get("/", response_model=List[Item])
async def read_all_items(
	updateInst: Optional[datetime] = None,
	uID: int = Depends(getUid), 
	session: AsyncSession = Depends(GetSession)
):
	return await crud.Item.read(session, uID, updateInst)

@router.get("/{ID}", response_model=Item)
async def read_item_by_ID(
	ID: int = Path(..., title="Item ID to fetch"),
	uID: int = Depends(getUid), 
	session: AsyncSession = Depends(GetSession)
):
	return await crud.Item.readID(session, uID, ID)

@router.put("/", status_code=status.HTTP_204_NO_CONTENT)
async def update_items(
	data_list: Dict[int, Dict] = None,
	uID: int = Depends(getUid), 
	session: AsyncSession = Depends(GetSession)
):
	result = await crud.Item.update(session, uID, data_list)
	if (not result): HTTPException(400, "User does not exist")
	session.commit()

@router.delete("/{ID}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_item(
	ID: int = Path(..., title="Item ID to delete"),
	uID: int = Depends(getUid), 
	session: AsyncSession = Depends(GetSession)
):
	result = await crud.Item.delete(session, uID, ID)
	if (not result): HTTPException(400, "User does not exist")
	session.commit()
