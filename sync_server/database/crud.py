from typing import List, Dict
from datetime import datetime
from pyotp import random_base32
from sqlalchemy import select, or_
from sqlalchemy.ext.asyncio import AsyncSession
from .model import User as UserDB
from .model import Item as ItemDB
from .schema import UserNew
from ..password import hash as pwdHash

async def _updateDBClass(schema, model):
	for key in schema.dict():
		value = getattr(schema, key)
		setattr(model, key, value)

class User:
	@staticmethod
	async def create(session: AsyncSession, user: UserNew) -> UserDB:
		if (user.name.isdigit()): return None
		
		result = await session.execute(select(UserDB).where(or_(UserDB.email == user.email, UserDB.name == user.name)))
		if (result.first()): return None
		user_dict: dict = user.dict()
		user_dict["password"] = await pwdHash(user_dict["password"])
		user_db = UserDB(**user_dict)
		session.add(user_db)
		await session.flush()
		return user_db

	@staticmethod
	async def readID(session: AsyncSession, ID: int) -> UserDB:
		user_db = await session.get(UserDB, ID)
		return user_db

	@staticmethod
	async def readName(session: AsyncSession, name: str) -> UserDB:
		result = await session.execute(select(UserDB).where(UserDB.name == name))
		return result.scalar_one_or_none()

	@staticmethod
	async def readEmail(session: AsyncSession, email: str) -> UserDB:
		result = await session.execute(select(UserDB).where(UserDB.email == email))
		return result.scalar_one_or_none()

	@staticmethod
	async def updateTotp(session: AsyncSession, ID: int) -> str:
		user_db: UserDB = await session.get(UserDB, ID)
		if not user_db: return None
		user_db.totp_secret = random_base32()
		return user_db.totp_secret

	@staticmethod
	async def update(session: AsyncSession, ID: int, user: UserNew) -> bool:
		user_db: UserDB = await session.get(UserDB, ID)
		if (user_db is None): return False
		await _updateDBClass(user, user_db)
		return True

	@staticmethod
	async def deleteTotp(session: AsyncSession, ID:int) -> bool:
		user_db: UserDB = await session.get(UserDB, ID)
		if (user_db is None): return False
		user_db.totp_secret = None
		return True

	@staticmethod
	async def delete(session: AsyncSession, ID: int) -> bool:
		user_db = await session.get(UserDB, ID)
		if (user_db is None): return False
		await session.delete(user_db)
		return True
	
class Item:
	@staticmethod
	async def create(session: AsyncSession, user_id: int, data_list: List[Dict]):
		item_db_list: List[ItemDB] = []
		for data in data_list:
			item_db_list.append(ItemDB(
				user_id = user_id,
				data = data
			))
		session.add_all(item_db_list)
		await session.flush()
		return item_db_list
		
	@staticmethod
	async def readID(session: AsyncSession, user_id: int, ID: int):
		item_db = await session.get(ItemDB, ID)
		if (not item_db): return None
		if (item_db.user_id != user_id): return None
		return item_db

	@staticmethod
	async def read(session: AsyncSession, user_id: int, time: datetime = None):
		query = select(ItemDB).where(ItemDB.user_id == user_id)
		if (time): query = query.where( ItemDB.update_inst >= time )
		result = await session.execute(query)
		return result.scalars().all()

	@staticmethod
	async def update(session: AsyncSession, user_id: int, item_list: Dict[int, Dict]):
		query = select(ItemDB).where(ItemDB.id.in_(item_list.keys())).where(ItemDB.user_id == user_id)
		result = await session.execute(query)
		item_db_list = result.scalars().all()
		if (len(item_db_list) != len(item_list)): return False
		await session.begin_nested()
		for id in item_list.keys():
			item = item_list[id]
			item_db_list_filter = list(filter(lambda x, key = id: x.id == key, item_db_list))
			if (len(item_db_list_filter) != 1): 
				await session.rollback()
				return False
			item_db_list_filter[0].data = item
		await session.commit()
		return True

	@staticmethod
	async def delete(session: AsyncSession, user_id: int, id: int):
		item_db = await session.get(ItemDB, id)
		if (item_db is None): return False
		if (item_db.user_id != user_id): return None
		await session.delete(item_db)
		return True
