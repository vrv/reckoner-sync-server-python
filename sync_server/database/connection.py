import asyncio
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine

DATABASE_URL = "sqlite+aiosqlite:///"
Base = declarative_base()
engine = create_async_engine(DATABASE_URL)#, echo=True)
async_session = sessionmaker(engine, class_=AsyncSession)

def GetEngine():
	return engine

def SetupConnection(db_config: dict):
	async def _Initialize():
		async with engine.begin() as conn:
			await conn.run_sync(Base.metadata.create_all)

	global DATABASE_URL
	global engine
	global async_session
	dialect: str = db_config.get("dialect", "sqlite")
	database: str = db_config.get("database", "database.db")
	if (dialect == "sqlite"):
		DATABASE_URL = "sqlite+aiosqlite:///" + database
	else:
		driver: str
		port: str
		if (dialect == "myslq"):
			driver = "aiomysql"
			port: str = db_config.get("port", "3360")
		elif (dialect == "postgres"):
			driver = "asyncpg"
			port: str = db_config.get("port", "5432")
		else:
			return

		host: str = db_config.get("host", "")
		username: str = db_config.get("username", "")
		password: str = db_config.get("password", "")

		#dialect+driver://username:password@host:port/database
		DATABASE_URL = dialect + "+" + driver + "://" + username + ":" + password + "@" + host + ":" + port + "/" + database
	
	engine = create_async_engine(DATABASE_URL)#, echo=True)
	async_session = sessionmaker(engine, class_=AsyncSession)
	asyncio.run_coroutine_threadsafe(_Initialize(), asyncio.get_event_loop())

async def GetSession():
	async with async_session() as session:
		yield session

async def Reset():
	async with engine.begin() as conn:
		await conn.run_sync(Base.metadata.drop_all)
		await conn.run_sync(Base.metadata.create_all)