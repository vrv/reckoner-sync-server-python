from typing import Optional, Dict
from datetime import datetime
from pydantic import BaseModel

class _UserBase(BaseModel):
	name: str
	email: str

class UserNew(_UserBase):
	password: str
	is_admin: Optional[bool] = False
	public_key: Optional[str] = None
	private_key: Optional[str] = None

class User(_UserBase):
	id: int
	totp_secret: Optional[str] = None

	class Config:
		orm_mode = True

class Item(BaseModel):
	id: int
	update_inst: datetime
	user_id: int
	data: Dict

	class Config:
		orm_mode = True

